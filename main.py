#!/usr/bin/env python3
from src.state_machine import StateMachine

import logging.handlers

if __name__ == '__main__':
    # Load map file here and parse it into node graph
    # Then init StateMachine with current position

    logging.basicConfig(filename='output.log', level=logging.DEBUG, format='%(message)s')

    logging.debug("Lets start")
    robot = StateMachine()
    robot.start()
