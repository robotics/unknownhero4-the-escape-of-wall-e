class Config:
    P = 0.6  # Proportional Component of the Controller
    I = 0.3  # Integral Component of the Controller
    D = 0.4  # Derivative Component of the Controller
    BLOCK_DISTANCE_DETECTION = 55  # Distance between robot and block in mm, should be greater than 50
    SPEED = 20  # Percentage of maximum speed the motors of the robot can turn at
    BLOCK_PUSH_SPEED = 10 # Percentage of maximum speed the motors should use while pushing a block
    MAP_PATH = "./src/navigation/asciimap"  # Path from root directory of the project to the map file
    LINE_LOST_THRESHOLD = 10  # Value between 0 and 800 used to determine if a line has been lost
    LINE_FOUND_THRESHOLD = 100  # Value between 0 and 800 used to determine if a line has been found
    JUNCTION_FOUND_THRESHOLD = 320  # Value between 0 and 800 used to determine if a junction has been found
    JUNCTION_FOUND_CORNER_THRESHOLD = 80  # Value between 0 and 100 used to determine if a corner is active
    LENGTH_PER_POINT = 0.5 # Length between each point of the asciimap
