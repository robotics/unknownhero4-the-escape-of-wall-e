import logging

from ev3dev2.button import Button
from ev3dev2.sound import Sound

from src.config import Config
from src.model.exceptions import NoPathFoundException, UnexpectedStatusException, UnexpectedMoveException
from src.model.move import Move
from src.model.state import State
from src.model.status import Status
from src.movement.robot_controller import RobotController
from src.navigation.pathfinder import Pathfinder


class StateMachine:
    positions = []
    moves = []
    state = State.RECALCULATE_MOVE
    robot = RobotController()
    pathfinder = Pathfinder(Config.MAP_PATH)
    btn = Button()

    def start(self):
        while True:
            # self.waitUntilButtonPressed()
            logging.debug("Working on " + self.state.name)
            if self.state == State.NEXT_MOVE:
                self.robot.sound.play_file("./src/sounds/R2sassy.wav", play_type=Sound.PLAY_NO_WAIT_FOR_COMPLETE)
                if len(self.moves) == 0:
                    self.state = State.FINISH
                    continue
                self.do_move(self.moves[0])
            elif self.state == State.RECALCULATE_MOVE:
                self.robot.sound.play_file("./src/sounds/hmm.wav", play_type=Sound.PLAY_NO_WAIT_FOR_COMPLETE)
                self.recalculate_moves()
            elif self.state == State.FINISH:
                self.robot.sound.play_file("./src/sounds/R2beeping.wav", play_type=Sound.PLAY_NO_WAIT_FOR_COMPLETE)
                break
        print("Route complete")

    def waitUntilButtonPressed(self):
        while not self.btn.any():
            pass

    def do_move(self, move):
        logging.debug("DO MOVE " + str(move))

        if move == Move.DRIVE_TO_NEXT_JUNCTION:
            status = self.robot.drive_to_next_junction(True)
            
            if status == Status.RED_BLOCK_ENCOUNTERED:
                self.update_map(self.positions[1].position, True)
            elif status == Status.BLUE_BLOCK_ENCOUNTERED:
                self.update_map(self.positions[1].position, False)

            self.positions.pop(0)
            self.pathfinder.robot_position = self.positions[0]
            self.pathfinder.print_map()
        elif move == Move.PUSH_BLOCK:
            logging.debug("Pushing block")
            status = self.robot.push_block()
        elif move == Move.TURN_LEFT:
            status = self.robot.turn_left()
            self.pathfinder.orientation = (self.pathfinder.orientation + 3) % 4
        elif move == Move.TURN_RIGHT:
            status = self.robot.turn_right()
            self.pathfinder.orientation = (self.pathfinder.orientation + 1) % 4
        elif move == Move.TURN_AROUND:
            status = self.robot.turn_around()
            self.pathfinder.orientation = (self.pathfinder.orientation + 2) % 4
        elif move == Move.DRIVE_BACKWARDS_TO_NEXT_JUNCTION:
            status = self.robot.drive_backwards_to_next_junction()
        else:
            logging.debug(move)
            raise UnexpectedMoveException

        if move is not Move.DRIVE_TO_NEXT_JUNCTION and status is not Status.SUCCESS:
            # Not exiting the program here will cause the above conditions to corrupt the pathfinder state!
            logging.debug(status)
            raise UnexpectedStatusException

        self.moves.pop(0)
        self.state = State.NEXT_MOVE

    def recalculate_moves(self):
        self.pathfinder.astar()

        if len(self.pathfinder.get_node_path()) == 0:
            self.robot.sound.play_file("./src/sounds/scream.wav", play_type=Sound.PLAY_NO_WAIT_FOR_COMPLETE)
            raise NoPathFoundException()

        self.moves = self.pathfinder.get_move_path()
        self.positions = self.pathfinder.get_node_path_relevant()
        self.state = State.NEXT_MOVE

    def update_map(self, position, pushable):
        move = self.pathfinder.direction_to_move[self.pathfinder.orientation]
        push_to_node = self.pathfinder.map_reader.get_node(position[0] + move[0], position[1] + move[1])

        if not pushable or push_to_node is None or push_to_node in self.pathfinder.get_node_path():
            self.pathfinder.map_reader.delete_node(self.pathfinder.map_reader.get_node(position[0], position[1]))
        else:
            self.pathfinder.map_reader.delete_node(
                self.pathfinder.map_reader.get_node(position[0] + 2 * move[0], position[1] + 2 * move[1]))
            self.moves = [self.moves[0], Move.PUSH_BLOCK, Move.DRIVE_BACKWARDS_TO_NEXT_JUNCTION] + self.moves[1:]
            self.state = State.NEXT_MOVE
            return

        self.state = State.RECALCULATE_MOVE
