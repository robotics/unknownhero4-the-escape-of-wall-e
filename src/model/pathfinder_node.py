class PathfinderNode:
    def __init__(self, parent=None, node=None):
        self.parent = parent
        self.node = node

        self.g = 0
        self.f = 0

    def __eq__(self, other):
        return self.node.position == other.node.position