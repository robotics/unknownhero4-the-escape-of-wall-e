class Node:
    def __init__(self):
        self.up = None
        self.down = None
        self.left = None
        self.right = None
        self.position = None

    def __str__(self):
        return "N:" + str(self.position)

    def isStraight(self):
        return (self.up is None
                and self.down is None
                and self.left is not None
                and self.right is not None) or (self.left is None
                                                and self.right is None
                                                and self.up is not None
                                                and self.down is not None)
