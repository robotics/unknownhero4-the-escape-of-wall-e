class NoPathFoundException(Exception):
    pass


class UnexpectedStatusException(Exception):
    pass


class UnexpectedMoveException(Exception):
    pass
