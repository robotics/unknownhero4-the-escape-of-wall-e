from enum import Enum


class Status(Enum):
    RED_BLOCK_ENCOUNTERED = "rbe"
    BLUE_BLOCK_ENCOUNTERED = "bbe"
    JUNCTION_ENCOUNTERED = "je"
    SUCCESS = "succ"
    LINE_LOST = "ll"
