import enum


class Move(enum.Enum):
    TURN_RIGHT = "right"
    TURN_LEFT = "left"
    TURN_AROUND = "around"
    PUSH_BLOCK = "push"
    DRIVE_TO_NEXT_JUNCTION = "drive"
    DRIVE_BACKWARDS_TO_NEXT_JUNCTION = "drive_backwards"
