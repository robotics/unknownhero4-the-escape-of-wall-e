from enum import Enum


class State(Enum):
    NEXT_MOVE = 1,
    RECALCULATE_MOVE = 2,
    FINISH = 3,
