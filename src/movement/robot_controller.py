from ev3dev2.display import Display
from ev3dev2.motor import MoveSteering
from ev3dev2.motor import OUTPUT_B, OUTPUT_C
from ev3dev2.sound import Sound

from src.config import Config
from src.movement.pid_controller import PIDController
from src.sensor.color_sensor import ColorSensor
from src.sensor.light_array_sensor import Light_Array_Sensor
from src.sensor.ultrasonic_sensor import UltrasonicSensor
from src.model.status import Status
import logging


class RobotController:
    def __init__(self):
        self.robot = MoveSteering(OUTPUT_B, OUTPUT_C)
        self.light_array_sensor = Light_Array_Sensor()
        self.pid_controller = PIDController(self.light_array_sensor, Config.P, Config.I, Config.D)

        self.ultrasonic_sensor = UltrasonicSensor()
        self.color_sensor = ColorSensor()

        self.display = Display()
        self.sound = Sound()

        self.sound.play_file("./src/sounds/r2d2a.wav", play_type=Sound.PLAY_NO_WAIT_FOR_COMPLETE)
        self.currently_pushing_block_between_junction = False

    def drive_to_next_junction(self, detect_block=False, speed=Config.SPEED) -> Status:
        unchanged_speed = speed
        self.robot.reset()
        # Drive a bit to avoid detecting starting junction as end junction
        if self.pid_controller.get_angle() == Status.JUNCTION_ENCOUNTERED:
            for i in range(3):
                while self.pid_controller.get_angle() == Status.JUNCTION_ENCOUNTERED:
                    self._drive(0, speed)
        self._stop()

        # Drive to next junction
        pid_result = self.pid_controller.get_angle()
        while pid_result is not Status.JUNCTION_ENCOUNTERED:
            if detect_block:
                # Check for block using color sensor
                distance = self.ultrasonic_sensor.get_distance()
                speed = max(5, min(Config.SPEED, distance / 5))
                if distance < Config.BLOCK_DISTANCE_DETECTION:
                    red, green, blue = self.color_sensor.getRGB()
                    self._stop()
                    if red > blue:
                        return Status.RED_BLOCK_ENCOUNTERED
                    else:
                        return Status.BLUE_BLOCK_ENCOUNTERED

            if pid_result == Status.LINE_LOST:
                self.drive_back_to_line()
            else:
                self._drive(-pid_result, speed)
            
            # logging.debug("Motor Rotations: " + str(self.robot.left_motor.rotations))
            
            pid_result = self.pid_controller.get_angle()
            
        self._stop()

        return Status.SUCCESS

    def drive_backwards_to_next_junction(self, speed=Config.SPEED) -> Status:
        # Get over current Junction.
        if self.pid_controller.get_angle() == Status.JUNCTION_ENCOUNTERED:
            for i in range(3):
                while self.pid_controller.get_angle() == Status.JUNCTION_ENCOUNTERED:
                    self._drive(0, -speed)
        self._stop()

        # Drive backwards to previous junction
        pid_result = self.pid_controller.get_angle()
        while pid_result is not Status.JUNCTION_ENCOUNTERED:
            self._drive(-pid_result, -speed)
            pid_result = self.pid_controller.get_angle()
            if pid_result == Status.LINE_LOST:
                self.drive_back_to_line()
        self._stop()

        return Status.SUCCESS

    def push_block(self) -> Status:
        # Drive to the detected Block
        self.drive_to_next_junction(speed=10)
        # Push the Block to the next junction
        self.drive_to_next_junction(speed=10)
        return Status.SUCCESS

    def drive_back_to_line(self) -> Status:
        while sum(self.pid_controller.light_array_sensor.get_input_values()) < Config.LINE_FOUND_THRESHOLD:
            self._drive_backward()
        self._stop()
        return Status.SUCCESS

    def turn_left(self) -> Status:
        self._turn_deg(-90)
        self._stop()
        return Status.SUCCESS

    def turn_right(self) -> Status:
        self._turn_deg(90)
        self._stop()
        return Status.SUCCESS

    def turn_around(self) -> Status:
        self._turn_deg(180)
        self._stop()
        return Status.SUCCESS

    def _turn_deg(self, deg):
        self.robot.on_for_degrees(70, Config.SPEED, deg * 3)

    def _drive(self, angle, speed=Config.SPEED):
        self.robot.on(max(-100, min(100, angle)), speed)

    def _drive_backward(self, speed=Config.SPEED):
        self.robot.on(0, -speed)

    def _stop(self):
        self.pid_controller.reset()
        self.robot.off()
