import time
from typing import Union

from src.config import Config
from src.model.status import Status


class PIDController:
    def __init__(self, light_array_sensor, p, i, d, line_point=0):
        self.previous_error = 0
        self.integral = 0
        self.previous_value = 0
        self.p = p
        self.i = i
        self.d = d
        self.line_point = line_point
        self.light_array_sensor = light_array_sensor
        self.time = 0

    def get_angle(self) -> Union[float, Status]:
        if self.time == 0:
            self.time = time.time()

        dt = time.time() - self.time
        position = self.get_position_of_line_on_sensor()

        if type(position) is Status:
            return position

        error = ((self.line_point - position) ** 2) * (1 if position < 0 else -1)
        self.integral = (self.integral * 0.7 + error * dt)
        derivation = (error - self.previous_error) / dt if dt != 0 else 0

        result = self.p * error + self.i * self.integral + self.d * derivation

        self.previous_error = error
        self.time = time.time()

        return result

    def get_position_of_line_on_sensor(self) -> Union[float, Status]:
        light_array_values = self.light_array_sensor.get_input_values()
        light_array_sum = sum(light_array_values)

        if light_array_sum > Config.JUNCTION_FOUND_THRESHOLD and (
                light_array_values[0] > Config.JUNCTION_FOUND_CORNER_THRESHOLD or
                light_array_values[7] > Config.JUNCTION_FOUND_CORNER_THRESHOLD):
            return Status.JUNCTION_ENCOUNTERED

        if light_array_sum < Config.LINE_LOST_THRESHOLD:
            return Status.LINE_LOST

        weighted_values = 0

        for index, value in enumerate(light_array_values):
            weighted_values += (index + 1) * value

        pos = (weighted_values / light_array_sum) - 4.5
        self.previous_value = pos

        return pos

    def reset(self):
        self.previous_error = 0
        self.previous_value = 0
        self.integral = 0
