import logging
from src.model.direction import Direction
from src.model.move import Move
from src.model.pathfinder_node import PathfinderNode
from src.navigation.map_reader import MazeReader


def _get_move_from_action(action: int):
    if action == 0:
        return Move.DRIVE_TO_NEXT_JUNCTION
    if action == 1:
        return Move.TURN_RIGHT
    if action == 2:
        return Move.TURN_AROUND
    if action == 3:
        return Move.TURN_LEFT


class Pathfinder:
    def __init__(self, path):
        self.node_path = []
        self.map_reader = MazeReader(path)
        self.robot_position = self.map_reader.start
        self.orientation = None
        self.moves_to_direction = {(0, -1): Direction.UP,
                                   (0, 1): Direction.DOWN,
                                   (1, 0): Direction.RIGHT,
                                   (-1, 0): Direction.LEFT
                                   }
        self.direction_to_move = {Direction.UP: (0, -1),
                                  Direction.DOWN: (0, 1),
                                  Direction.RIGHT: (1, 0),
                                  Direction.LEFT: (-1, 0)
                                  }

    def get_node_path(self):
        return self.node_path

    def get_node_path_relevant(self):
        if len(self.get_node_path()) == 0:
            return []

        node_path = self.get_node_path()
        nodes = []

        nodes.append(self.robot_position)

        for i in range(1, len(node_path)):
            if not node_path[i].isStraight() or node_path[i] == self.map_reader.end:
                nodes.append(node_path[i])

        return nodes

    def get_direction_path(self):
        if len(self.get_node_path()) == 0:
            return []

        node_path = self.get_node_path()
        previous_position = node_path[0].position
        directions = []
        for i in range(1, len(node_path)):
            if not node_path[i].isStraight():
                directions.append(
                    self.moves_to_direction[
                        (node_path[i].position[0] - previous_position[0],
                         node_path[i].position[1] - previous_position[1])])
            previous_position = node_path[i].position
        return directions

    def get_move_path(self):
        moves = [Move.DRIVE_TO_NEXT_JUNCTION]
        direction_path = self.get_direction_path()

        for i in range(len(direction_path) - 1):
            # 0 drive forward
            # 1 turn right
            # 2 turn around
            # 3 turn left
            action = (direction_path[i + 1] - direction_path[i]) % 4
            moves.append(_get_move_from_action(action))
            if action != 0:
                moves.append(Move.DRIVE_TO_NEXT_JUNCTION)
        return moves

    def astar(self):
        start = PathfinderNode(None, self.robot_position)
        goal = PathfinderNode(None, self.map_reader.end)

        open_list = []
        closed_list = []

        open_list.append(start)

        while len(open_list) > 0:
            current_node = open_list[0]
            current_index = 0
            for index, item in enumerate(open_list):
                if item.f < current_node.f:
                    current_node = item
                    current_index = index

            open_list.pop(current_index)
            closed_list.append(current_node)

            if current_node == goal:
                self.node_path = []
                current = current_node
                while current is not None:
                    self.node_path.append(current.node)
                    current = current.parent
                self.node_path = self.node_path[::-1]

                if self.orientation is None:
                    if self.node_path[0].up == self.node_path[1]:
                        self.orientation = Direction.UP
                    elif self.node_path[0].down == self.node_path[1]:
                        self.orientation = Direction.DOWN
                    elif self.node_path[0].left == self.node_path[1]:
                        self.orientation = Direction.LEFT
                    elif self.node_path[0].right == self.node_path[1]:
                        self.orientation = Direction.RIGHT
                return

            children = []
            if current_node.node.up is not None:
                children.append(PathfinderNode(current_node, current_node.node.up))
            if current_node.node.down is not None:
                children.append(PathfinderNode(current_node, current_node.node.down))
            if current_node.node.left is not None:
                children.append(PathfinderNode(current_node, current_node.node.left))
            if current_node.node.right is not None:
                children.append(PathfinderNode(current_node, current_node.node.right))

            for child in children:
                closed_list_member = False
                for closed_child in closed_list:
                    if child == closed_child:
                        closed_list_member = True
                        break
                if closed_list_member:
                    continue
                child.g = current_node.g + 1
                h = ((child.node.position[0] - goal.node.position[0]) ** 2) + (
                        (child.node.position[1] - goal.node.position[1]) ** 2)
                child.f = child.g + h

                open_list_better_member = False
                for open_node in open_list:
                    if child == open_node and child.g > open_node.g:
                        open_list_better_member = True
                        break
                if open_list_better_member:
                    continue

                open_list.append(child)
    
    def print_map(self):
        logging.debug("----------------------------------")

        for y in range(len(self.map_reader.nodes)):
            row = self.map_reader.nodes[y]
            
            line = ""
            for x in range(len(row)):
                prefix = '\033[91m' if row[x] in self.get_node_path() else ""
                if (x, y) == self.robot_position.position:
                    line+= prefix + "X" + '\033[0m'
                elif row[x] is None:
                    line+=" "
                else:
                    line+= prefix + "*" + '\033[0m'
            
            logging.debug(line)
        
        logging.debug("----------------------------------")

if __name__ == '__main__':
    map = Pathfinder("asciimap")
    map.astar()
    print(*map.get_node_path())
    print(map.get_direction_path())
    print(*map.get_node_path_relevant())
    print(map.get_move_path())
