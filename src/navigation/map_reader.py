from src.model.node import Node


class MazeReader:
    def __init__(self, path):
        self.start = None
        self.end = None
        self.nodes = []
        maze = open(path, "r").read()
        maze = maze.split("\n")
        for y in range(len(maze)):
            row = []
            for x in range(len(maze[y])):
                if maze[y][x] == ' ':
                    row.append(None)
                else:
                    row.append(Node())
                    if maze[y][x] == 'G':
                        self.end = row[x]
                    elif maze[y][x] == 'S':
                        self.start = row[x]
            self.nodes.append(row)
        for y, row in enumerate(self.nodes):
            for x, node in enumerate(row):
                if node is None:
                    continue
                node.up = self.get_node(x, y - 1)
                node.down = self.get_node(x, y + 1)
                node.left = self.get_node(x - 1, y)
                node.right = self.get_node(x + 1, y)
                node.position = (x, y)

    def get_node(self, x, y):
        if x < 0 or y < 0:
            return None
        try:
            return self.nodes[y][x]
        except IndexError:
            return None

    def delete_node(self, node):
        if node is not None:
            if node.up is not None:
                node.up.down = None
            if node.down is not None:
                node.down.up = None
            if node.left is not None:
                node.left.right = None
            if node.right is not None:
                node.right.left = None


if __name__ == '__main__':
    maze = MazeReader("asciimap")
    mazetest = ""
    for y, row in enumerate(maze.nodes):
        line = ""
        for x, node in enumerate(row):
            if node is None:
                line += " "
            else:
                line += "*"
        mazetest += line + "\n"
    print(mazetest)
