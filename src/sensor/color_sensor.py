from typing import Tuple
from ev3dev2.sensor import INPUT_3
from ev3dev2.sensor.lego import ColorSensor as CSensor


class ColorSensor:
    def __init__(self):
        self.sensor = CSensor(INPUT_3)

    def getColor(self) -> str:
        # Either 'NoColor','Black','Blue','Green','Yellow','Red','White','Brown',
        return self.sensor.color_name()

    def getRGB(self) -> Tuple[int, int, int]:
        return self.sensor.rgb
