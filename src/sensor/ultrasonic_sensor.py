from ev3dev2.sensor import INPUT_2
from ev3dev2.sensor.lego import UltrasonicSensor as USensor


class UltrasonicSensor:
    def __init__(self):
        self.sensor = USensor(INPUT_2)

    def get_distance(self) -> float:
        return self.sensor.value()
