from ev3dev2.sensor import INPUT_1, Sensor
import logging


class Light_Array_Sensor:
    def __init__(self):
        self.sensor = Sensor(INPUT_1)
        self.sensor.mode = "CAL"

    def get_input_value(self, number):
        return 100 - self.sensor.value(number)

    def get_input_values(self):
        values = [0] * 8
        found = False
        left = 3
        right = 4

        while left >= 0 and right <= 7:
            if not found:
                values[left] = self.get_input_value(left)
                values[right] = self.get_input_value(right)

                if values[left] != 0 or values[right] != 0:
                    found = True

                left -= 1
                right += 1
                continue

            if values[left + 1] != 0:
                values[left] = self.get_input_value(left)
                left -= 1

            if values[right - 1] != 0:
                values[right] = self.get_input_value(right)
                right += 1

            if values[right - 1] == 0 and values[left + 1] == 0:
                break

        # logging.debug("LAS Values: " + str(values))
        return values
